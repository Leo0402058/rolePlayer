

import javafx.beans.property.StringProperty;

/**
 *
 * @author User
 */
public class dataBean {
    private StringProperty characterName;
    private StringProperty actorName;

    public String getCharacterName() {
        return characterName.get();
    }

    public void setCharacterName(String characterName) {
        this.characterName.set(characterName);
    }
    
    public StringProperty characterNameProperty(){
        
        return characterName;
    }

    public String getActorName() {
        return actorName.get();
    }

    public void setActorName(String actorName) {
        this.actorName.set(actorName);
    }
    
    public StringProperty actorNameProperty(){
    
             return actorName;
    }

    @Override
    public String toString() {
        return "The role of " + characterName.get() + " will be played by" + actorName.get();
    }
    
    
}
