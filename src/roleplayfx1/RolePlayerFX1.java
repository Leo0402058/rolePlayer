package roleplayfx1;

import javafx.application.Application;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class RolePlayerFX1 extends Application {

    private DataBean databean;
    private RolePlayerGUI gui;

    @Override
    public void init() {
        databean = new DataBean();

        gui = new RolePlayerGUI();
    }

    @Override
    public void start(Stage primaryStage) {

        gui.start(primaryStage, databean);
    }

    public static void main(String[] args) {

        Application.launch(args);

    }

}
