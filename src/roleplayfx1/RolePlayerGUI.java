package roleplayfx1;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import javafx.beans.binding.Bindings;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 *
 * @author User
 */
public class RolePlayerGUI {

    private Label characterlbl1;
    private Label actorlbl1;
    private Label characterlbl2;
    private Label actorlb2;
    private Label lbl1;
    private Label lbl2;
    private TextField characterText;

    private TextField actorText;
    private Text message;
    private DataBean databean;

//    public void RolePlayerGUI(DataBean databean) {
//        this.databean = databean;
//        initialize();
//
//    }
    public void start(Stage primaryStage, DataBean databean) {
        this.databean = databean;

        GridPane grid = new GridPane();

        characterlbl1 = new Label("Character's Name");
        grid.add(characterlbl1, 0, 0);

        characterText = new TextField();
        grid.add(characterText, 1, 0);

        actorlbl1 = new Label("Actor's Name");
        grid.add(actorlbl1, 0, 1);

        actorText = new TextField();
        grid.add(actorText, 1, 1);

        lbl1 = new Label("The role of ");
        lbl2 = new Label("is played by ");

        HBox hbbox = new HBox();
        hbbox.getChildren().addAll(lbl1, characterlbl1, lbl2, actorlbl1);
        grid.add(hbbox, 0, 2);
        initialize();

        primaryStage.setTitle("Game");
        //GridPane root = createUserInterface();
        Scene scene = new Scene(grid, 300, 275);
        primaryStage.setScene(scene);
        primaryStage.show();

    }

    public void initialize() {
        Bindings.bindBidirectional(characterText.textProperty(), lbl1.textProperty());
        Bindings.bindBidirectional(actorText.textProperty(), lbl2.textProperty());
    }
}
